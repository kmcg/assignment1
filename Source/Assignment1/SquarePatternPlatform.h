// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SquarePatternPlatform.generated.h"

UCLASS()
class ASSIGNMENT1_API ASquarePatternPlatform : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASquarePatternPlatform();

	UPROPERTY(EditAnywhere)
		float squareLength = 1000.0f;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* platformMesh;
	UPROPERTY(EditAnywhere)
		float speed = 100.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FVector startPosition;
	float xSpeed;
	float ySpeed;

	float xPos;
	float yPos;

	float yMax;
	float yMin;

	float xMax;
	float xMin;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
