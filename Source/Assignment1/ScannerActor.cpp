// Fill out your copyright notice in the Description page of Project Settings.


#include "ScannerActor.h"

// Sets default values
AScannerActor::AScannerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	staticMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AScannerActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AScannerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FHitResult linetraceResult;
	// Stat position of check
	FVector startTrace = GetActorLocation();
	startTrace += GetActorForwardVector() * 100.0f;
	FVector endTrace = (GetActorForwardVector() * 300.0f) + startTrace;
	FCollisionQueryParams params;
	bool isHit = GetWorld()->LineTraceSingleByChannel(linetraceResult, startTrace, endTrace,
		ECC_WorldStatic, params);
	if (isHit) {
		
		AAssignment1Character* shootTarget = Cast<AAssignment1Character>(linetraceResult.GetActor());
		if (shootTarget) {
			targetPlatform->StartMove();
		}
	}
}

