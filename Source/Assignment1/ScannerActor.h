// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Assignment1Character.h"
#include "TriggerPlatformActor.h"
#include "GameFramework/Actor.h"
#include "ScannerActor.generated.h"

UCLASS()
class ASSIGNMENT1_API AScannerActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AScannerActor();

	UPROPERTY(EditAnywhere)
		ATriggerPlatformActor* targetPlatform;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* staticMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
