// Fill out your copyright notice in the Description page of Project Settings.


#include "TriggerPlatformActor.h"

// Sets default values
ATriggerPlatformActor::ATriggerPlatformActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	staticMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATriggerPlatformActor::BeginPlay()
{
	Super::BeginPlay();
	startPosition = GetActorLocation();
	SetActorLocation(FVector(GetActorLocation().X, startPosition.Y + distance, GetActorLocation().Z));
}

// Called every frame
void ATriggerPlatformActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GetActorLocation().Y > startPosition.Y + distance) {
		SetActorLocation(FVector(GetActorLocation().X, startPosition.Y + distance, GetActorLocation().Z));
		isMoving = false;
		currentSpeed = 0.0f;
	}

	if (GetActorLocation().Y < startPosition.Y - distance) {
		SetActorLocation(FVector(GetActorLocation().X, startPosition.Y - distance, GetActorLocation().Z));
		isMoving = false;
		currentSpeed = 0.0f;
	}

	SetActorLocation(GetActorLocation() + FQuat(GetActorRotation()) * FVector(currentSpeed*DeltaTime, 0.0f, 0.0f));

}

void ATriggerPlatformActor::StartMove()
{
	if (isMoving) {
		return;
	}

	isMoving = true;

	if (GetActorLocation().Y > startPosition.Y) {
		currentSpeed = -speed;
	}
	else {
		currentSpeed = speed;
	}
}

