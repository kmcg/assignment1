// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TriggerPlatformActor.generated.h"

UCLASS()
class ASSIGNMENT1_API ATriggerPlatformActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATriggerPlatformActor();
	UPROPERTY(EditAnywhere)
		float speed = 300.0f;

	UPROPERTY(EditAnywhere)
		float distance = 500.0f;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* staticMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool isMoving = false;
	float currentSpeed = 0.0f;

	FVector startPosition;

public:	


	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void StartMove();

};
