// Fill out your copyright notice in the Description page of Project Settings.


#include "ShootableActor.h"

// Sets default values
AShootableActor::AShootableActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	mVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));

	mVisibleComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AShootableActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AShootableActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	jumpTimer += DeltaTime;

	if (GetActorLocation().Z >= groundLevel) {
		verticalSpeed -= jumpSpeed * 2 * DeltaTime;
	}
	else {
		verticalSpeed = 0.0f;
	}

	if (jumpTimer >= jumpInterval) {
		jumpTimer = 0.0f;

		verticalSpeed = jumpSpeed;
	}

	if (FMath::RandRange(0, 100) >= 99) {
		if (GetActorLocation().Z <= groundLevel) {
			currentSpeed *= -1;
		}
	}

	if (GetActorLocation().Y > maxY) {
		if (GetActorLocation().Z <= groundLevel) {
			currentSpeed = -maxSpeed;
		}
	}
	else if (GetActorLocation().Y < minY) {
		if (GetActorLocation().Z <= groundLevel) {
			currentSpeed = maxSpeed;
		}
	}
	FVector movementDelta;
	movementDelta.Y = currentSpeed;
	movementDelta.Z = verticalSpeed;

	SetActorLocation(GetActorLocation() + movementDelta * DeltaTime);
}

void AShootableActor::OnBulletHit()
{
	Destroy();
}

