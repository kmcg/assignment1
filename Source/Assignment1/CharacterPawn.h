// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Pawn.h"
#include "CollisionQueryParams.h"
#include "ShootableActor.h"
#include "CharacterPawn.generated.h"

UCLASS()
class ASSIGNMENT1_API ACharacterPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACharacterPawn();
	UPROPERTY(EditAnywhere)
		float speed = 500.0f;

protected:

	FVector mMovementInput;

	UCameraComponent* mCamera;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float axisValue);
	void MoveRight(float axisValue);
	void Shoot();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
