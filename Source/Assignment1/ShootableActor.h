// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ShootableActor.generated.h"

UCLASS()
class ASSIGNMENT1_API AShootableActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShootableActor();

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* mVisibleComponent;
	UPROPERTY(EditAnywhere)
		float maxSpeed = 300.0f;

	UPROPERTY(EditAnywhere)
		float maxY;
	UPROPERTY(EditAnywhere)
		float minY;

	UPROPERTY(EditAnywhere)
		float jumpInterval = 3.0f;
	UPROPERTY(EditAnywhere)
		float jumpSpeed = 100.0f;
	UPROPERTY(EditAnywhere)
		float groundLevel = 130.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	float currentSpeed = maxSpeed;
	float jumpTimer = 0.0f;
	float verticalSpeed = 0.0f;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void OnBulletHit();
};
