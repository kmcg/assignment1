// Fill out your copyright notice in the Description page of Project Settings.


#include "SquarePatternPlatform.h"

// Sets default values
ASquarePatternPlatform::ASquarePatternPlatform()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	platformMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Platform"));
	platformMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ASquarePatternPlatform::BeginPlay()
{
	Super::BeginPlay();
	startPosition = GetActorLocation();
	SetActorLocation(FVector(startPosition.X + (squareLength / 2), startPosition.Y + (squareLength / 2), startPosition.Z));
	xSpeed = speed;
}

// Called every frame
void ASquarePatternPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector currentPosition = GetActorLocation();

	if (currentPosition.X > startPosition.X + (squareLength / 2)) {
		currentPosition.X = startPosition.X + (squareLength / 2);
		ySpeed = speed;
		xSpeed = 0.0f;
	}
	if (currentPosition.X < startPosition.X - (squareLength / 2)) {
		currentPosition.X = startPosition.X - (squareLength / 2);
		ySpeed = -speed;
		xSpeed = 0.0f;
	}

	if (currentPosition.Y > startPosition.Y + (squareLength / 2)) {
		currentPosition.Y = startPosition.Y + (squareLength / 2);
		ySpeed = 0.0f;
		xSpeed = -speed;
	}
	if (currentPosition.Y < startPosition.Y - (squareLength / 2)) {
		currentPosition.Y = startPosition.Y - (squareLength / 2);
		ySpeed = 0.0f;
		xSpeed = speed;
	}

	currentPosition += FVector(xSpeed, ySpeed, 0.0f) * DeltaTime;
	SetActorLocation(currentPosition);
}

